from alt:p10

ARG BUILD_DATE=2023-01-18

RUN apt-get update && \
    apt-get install -y \
    clang15.0-tools \
    colordiff \
    git \
    pip \
    python3 \
    python3-module-lxml \
    qt6-declarative-devel \
    qt6-tools \
    && \
    pip3 install langdetect && \
    apt-get clean && \
    ln -sf /usr/bin/qmlformat-qt6 /usr/local/bin/qmlformat

ENV ALTWRAP_LLVM_VERSION=15.0

COPY arguments.py \
    aurora_tools_common.py \
    check-code.py \
    check-cpp-code.py \
    check-md-files.py \
    check-pro-files.py \
    check-qml-code.py \
    check-translation.py \
    clang-format \
    colorcodes.py \
    format-cpp-code.py \
    format-qml-code.py \
    license-format.py \
    license-template \
    remove-executable-bit.py \
    streamformatter.py \
    /usr/local/bin/
