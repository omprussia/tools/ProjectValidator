import unittest
import sys
import os

os.chdir(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.abspath(".."))
license_format = __import__("license-format")

C_QML_DART_OLD_HEADER = """/*
* Test application
*/
"""
C_QML_DART_NEW_HEADER = """// Test application"""
PRO_HEADER = """# Test application"""
C_QML_DART_CODE = """#include <iostream>
#include <cstdlib>
using namespace std;

int main()
{
    cout << 'Hello, world!' << endl;
    return 0;
}
"""
PRO_CODE = """QT += sql core
TARGET = test-application
CONFIG += auroraapp
"""


class LicenseFormatTests(unittest.TestCase):
    def test_delete_old_c_and_qml_license(self):
        code = C_QML_DART_CODE.splitlines()
        content = C_QML_DART_OLD_HEADER.splitlines() + code
        license_format.delete_old_header(
            content,
            license_format.SETTINGS["c_and_qml"]["regex_start"],
            license_format.SETTINGS["c_and_qml"]["regex_end"],
        )
        self.assertEqual(content, code)

    def test_delete_new_c_and_qml_license(self):
        code = C_QML_DART_CODE.splitlines()
        content = C_QML_DART_NEW_HEADER.splitlines() + code
        license_format.delete_old_header(
            content,
            license_format.SETTINGS["c_and_qml"]["regex_start"],
            license_format.SETTINGS["c_and_qml"]["regex_end"],
        )
        self.assertEqual(content, code)

    def test_delete_pro_license(self):
        code = PRO_CODE.splitlines()
        content = PRO_HEADER.splitlines() + code
        license_format.delete_old_header(
            content,
            license_format.SETTINGS["pro"]["regex_start"],
            license_format.SETTINGS["pro"]["regex_end"],
        )
        self.assertEqual(content, code)


if __name__ == "__main__":
    unittest.main()
