import subprocess


class Colorcodes(object):
    """
    Provides ANSI terminal color codes which are gathered via the ``tput``
    utility. That way, they are portable. If there occurs any error with
    ``tput``, all codes are initialized as an empty string.

    The provides fields are listed below.

    Control:

    - bold
    - reset

    Colors:

    - red
    - orange
    - blue
    - green

    :license: MIT
    """

    def __init__(self):
        self.bold = self.__get_terminal_symbol("bold")
        self.reset = self.__get_terminal_symbol("sgr0")
        self.red = self.__get_terminal_symbol("setaf 1")
        self.green = self.__get_terminal_symbol("setaf 2")
        self.orange = self.__get_terminal_symbol("setaf 3")
        self.blue = self.__get_terminal_symbol("setaf 4")
        self.error = self.red_str("Error.")
        self.warning = self.orange_str("Warning.")

    def __get_terminal_symbol(self, tput_arguments: str) -> str:
        try:
            return subprocess.check_output(
                f"tput {tput_arguments}".split(), stderr=subprocess.DEVNULL
            ).decode()
        except subprocess.CalledProcessError:
            return ""

    def red_str(self, string):
        return self.color_str(self.red, string)

    def orange_str(self, string):
        return self.color_str(self.orange, string)

    def color_str(self, color_code, string):
        """
        :param color_code: Terminal color code
        :param string: String to wrap with the color
        :return: String wrapped with starting color code and reset symbol
        """
        return color_code + string + self.reset


colorcodes = Colorcodes()
