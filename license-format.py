#!/usr/bin/env python3

import argparse
import os
import sys
import re
import logging.config
from aurora_tools_common import (
    check_directory,
    list_files,
    add_default_except_dir,
    set_log_level,
    LOGGER_DICT,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
    add_check_license_argument,
    add_year_argument,
    add_replace_license_argument,
    add_license_path_argument,
)

DEFAULT_LICENSE_PATH = f"{os.path.dirname(__file__)}/license-template"
SETTINGS = {
    "c_and_qml": {
        "extensions": [
            ".h",
            ".cpp",
            ".c",
            ".cc",
            ".cs",
            ".qml",
            ".kt",
            ".kts",
            ".js",
            ".cjs",
            ".mjs",
            ".php",
        ],
        "file_names": (),
        "regex_start": [r"\/\/+", r"\/\*+"],
        "regex_end": [r"\/\/+", r"\*+\/"],
        "regex_date": r"\/\/ .*\d{4}(-\d{4})?.*",
        "affix": {
            "middle": "// ",
        },
    },
    "pro": {
        "extensions": [".pro", ".pri", ".yaml", ".yml", ".sh"],
        "file_names": ["CMakeLists.txt"],
        "regex_start": [r"#+"],
        "regex_end": [r"#+"],
        "regex_date": r"# .*\d{4}(-\d{4})?.*",
        "affix": {
            "middle": "# ",
        },
    },
    "dart": {
        "extensions": [".dart"],
        "file_names": (),
        "regex_start": [r"\/\/+"],
        "regex_end": [r"\/\/+"],
        "regex_date": r"\/\/ .*\d{4}(-\d{4})?.*",
        "affix": {
            "middle": "// ",
        },
    },
}

logging.config.dictConfig(LOGGER_DICT)
logger = logging.getLogger(__name__)


def main():
    arguments = parse_arguments()
    if arguments.log_level:
        set_log_level(logger, arguments.log_level)
    if arguments.license_path != DEFAULT_LICENSE_PATH:
        if not os.path.exists(arguments.license_path):
            logger.warning("File with license text not found by given path.")
            logger.info("Trying to use license file by default path...")
            if os.path.exists(DEFAULT_LICENSE_PATH):
                arguments.license_path = DEFAULT_LICENSE_PATH
            else:
                logger.error(
                    "File with license text not found in default path.",
                )
                return 1
    else:
        if not os.path.exists(DEFAULT_LICENSE_PATH):
            logger.error(
                "File with license text not found in default path. Provide path with license text with --license-path argument.",
            )
    if arguments.directory:
        check_directory(arguments.directory)
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    license_text = get_license_from_file(arguments.license_path, arguments.year)
    code = 0
    for configuration in SETTINGS.values():
        header = assemble_title(license_text, configuration["affix"])
        files = list_files(
            base_directory,
            except_data.directories,
            except_data.files,
            configuration["extensions"],
            configuration["file_names"],
        )
        if not len(files) == 0:
            if not check_file_headers(
                files,
                arguments.check_license,
                arguments.replace_license,
                header,
                configuration,
            ):
                code = 1
    return code


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to check files license header"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    add_check_license_argument(parser)
    add_year_argument(parser)
    add_replace_license_argument(parser)
    add_license_path_argument(parser)
    return parser.parse_args()


def check_file_headers(files, only_check, is_replace, header, settings):
    for file in files:
        file_header = []
        is_equal = True
        with open(file) as opened_file:
            num_lines = sum(1 for _ in opened_file)
            if num_lines >= len(header):
                opened_file.seek(0, 0)
                for _ in range(len(header)):
                    line = next(opened_file).strip()
                    file_header.append(line)
                if header == file_header:
                    if any(
                        re.match(settings["regex_date"], line.strip())
                        for line in file_header
                    ):
                        logger.info(f"{file}: correct header.")
                    else:
                        is_equal = False
                        logger.warning(
                            f"{file}: incorrect header. There is no matching date in header.",
                        )
                else:
                    is_equal = False
                    logger.error(f"{file}: incorrect header.")
            else:
                is_equal = False
                logger.error(
                    f"{file}: incorrect header. Not enough lines in file for header.",
                )
        if only_check is False and is_equal is False:
            write_header_by_regex(file, header, settings, is_replace)
    return True


def write_header_by_regex(file, header, settings, should_replace):
    file_name = os.path.basename(file)
    logger.info(f"Start writing the license header to a file {file_name}...")
    if write_header_in_file(
        file,
        header,
        should_replace,
        settings["regex_start"],
        settings["regex_end"],
    ):
        logger.info(f"Header is written to a file {file_name}.")
        return True
    else:
        logger.error(f"Error while writing header in {file_name}.")
        return False


def assemble_title(license_text, affix):
    header = []
    for i in range(len(license_text)):
        header.append((affix["middle"] + license_text[i]).strip())
    return header


def get_license_from_file(path, year):
    newline = "\n"
    text = []
    with open(path, "r") as file:
        file.seek(0, 0)
        for line in file:
            text.append(f"{line.replace(newline, '')}".format(**locals()))
    return text


def write_header_in_file(file, header, should_replace, start_regex, end_regex):
    with open(file, "r+") as opened_file:
        content = opened_file.read().split("\n")
        if should_replace:
            delete_old_header(content, start_regex, end_regex)
        opened_file.seek(0, 0)
        opened_file.truncate(0)
        opened_file.write("\n".join(header))
        opened_file.write("\n")
        opened_file.write("\n".join(content))
    return True


def delete_old_header(content, start_regex, end_regex):
    header_end = -2
    for i in range(len(start_regex)):
        if re.match(start_regex[i], content[0].strip()):
            if start_regex[i] == end_regex[i]:
                for j, line in enumerate(content):
                    if not re.match(start_regex[i], line.strip()):
                        header_end = j - 1
                        break
            else:
                end_line = content[0].strip()
                end_line = end_line[1:] + end_line[0]
                header_end = (
                    content.index(end_line, 1) if end_line in content[1:] else -1
                )
        if header_end != -1 and header_end != -2:
            del content[: header_end + 1]
            break
    if header_end == -1:
        logger.warning(
            "Deleting old header failed: end of old header was not found.",
        )
    elif header_end == -2:
        logger.warning(
            "Deleting old header failed: start of old header was not found in first line of file.",
        )
    return content


if __name__ == "__main__":
    sys.exit(main())
