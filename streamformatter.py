import logging
from colorcodes import colorcodes


class StreamFormatter(logging.Formatter):
    def format(self, record):
        if record.levelno == logging.INFO:
            self._style._fmt = "%(message)s"
        elif record.levelno == logging.WARNING:
            self._style._fmt = colorcodes.warning + " %(message)s"
        elif record.levelno == logging.ERROR:
            self._style._fmt = colorcodes.error + " %(message)s"
        return super().format(record)
