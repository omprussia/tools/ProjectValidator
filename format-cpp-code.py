#!/usr/bin/env python3

import argparse
import os
import sys
import subprocess
from aurora_tools_common import (
    check_directory,
    list_files,
    CLANG_FORMAT,
    change_ownership,
    get_clang_format_path,
    add_default_except_dir,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_cpp_dir_argument,
    add_fix_ownership_argument,
)


def main():
    arguments = parse_arguments()
    if arguments.directory:
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    cpp_files = list_files(
        base_directory,
        except_data.directories,
        except_data.files,
        [
            ".h",
            ".cpp",
            ".c",
            ".cc",
            ".cs",
        ],
        subdirectories=arguments.cpp_dir,
    )
    if len(cpp_files) == 0:
        return 0
    clang_format_path = get_clang_format_path(base_directory)
    result = format_cpp_files(cpp_files, clang_format_path)
    if arguments.fix_ownership:
        change_ownership(base_directory, *cpp_files)
    if result:
        return 0
    return 1


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to format code with clang-format"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_cpp_dir_argument(parser)
    add_fix_ownership_argument(parser)
    return parser.parse_args()


def format_cpp_files(cpp_files, clang_format_path):
    style = f"-style=file:{clang_format_path}"
    result = subprocess.run(
        [
            CLANG_FORMAT,
            style,
            "-i",
            *cpp_files,
        ]
    )
    return result.returncode == 0


if __name__ == "__main__":
    sys.exit(main())
