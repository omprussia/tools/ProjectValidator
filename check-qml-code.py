#!/usr/bin/env python3

import argparse
import logging
import os
import sys
import subprocess
import tempfile
from aurora_tools_common import (
    check_directory,
    list_files,
    QML_FORMAT,
    add_default_except_dir,
    set_log_level,
    LOGGER_DICT,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
    add_qml_dir_argument,
)

logging.config.dictConfig(LOGGER_DICT)
logger = logging.getLogger(__name__)


def main():
    arguments = parse_arguments()
    if arguments.log_level:
        set_log_level(logger, arguments.log_level)
    if arguments.directory:
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    qml_files = list_files(
        base_directory,
        except_data.directories,
        except_data.files,
        [".qml"],
        subdirectories=arguments.qml_dir,
    )
    if len(qml_files) == 0:
        return 0
    if check_qml_files(qml_files):
        return 0
    return 1


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to check code with qmlformat"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    add_qml_dir_argument(parser)
    return parser.parse_args()


def check_qml_files(qml_files):
    version_info = subprocess.run([QML_FORMAT, "--version"], capture_output=True)
    logger.info(
        f"Checking with {version_info.stdout.decode('utf-8')}.".replace("\n", "")
    )
    overall_result = True
    for qml_file in qml_files:
        logger.info(f"Checking file {qml_file}.")
        format_result = subprocess.run(
            [
                QML_FORMAT,
                qml_file,
            ],
            capture_output=True,
        )
        with tempfile.NamedTemporaryFile(delete=False) as temp_file:
            temp_file.write(format_result.stdout)
            temp_file.close()
            result = subprocess.run(
                [
                    "colordiff",
                    qml_file,
                    temp_file.name,
                ],
            )
            os.remove(temp_file.name)
            overall_result = overall_result and (result.returncode == 0)
    return overall_result


if __name__ == "__main__":
    sys.exit(main())
