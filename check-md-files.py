#!/usr/bin/env python3

import argparse
import os
import sys
import re
import langdetect
import logging.config
from langdetect import DetectorFactory
from aurora_tools_common import (
    check_directory,
    list_files,
    add_default_except_dir,
    set_log_level,
    LOGGER_DICT,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
)

FILE_NAMES = {
    "authors": "AUTHORS.md",
    "code_of_conduct": "CODE_OF_CONDUCT.md",
    "contributing": "CONTRIBUTING.md",
    "license": "LICENSE.BSD-3-Clause.md",
    "readme": "README.md",
}
REGEX = {
    "author_start": r"\* .*",
    "author": r"\*\s+[A-Z][a-z]+ [A-Z][a-z]+(, <.+@omp.ru>)?",
    "role": r"\s{2}\*\s[A-Z][a-z]+( [a-z]+)?,\s+\d{4}(-\d{4})?",
    "copyright": r".*Copyright( \([C|c]\))? \d{4}(-\d{4})?.*",
    "sub_special": "\W+",
}
AUTHORS_HEADER = "# Authors"

logging.config.dictConfig(LOGGER_DICT)
logger = logging.getLogger(__name__)


def main():
    arguments = parse_arguments()
    if arguments.log_level:
        set_log_level(logger, arguments.log_level)
    if arguments.directory:
        check_directory(arguments.directory)
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    files = list_files(
        base_directory,
        except_data.directories,
        except_data.files,
        [".md"],
        FILE_NAMES.values(),
    )
    found_file_names = []
    code = 0
    for file in files:
        name = os.path.basename(file)
        found_file_names.append(name)
        logger.info(f"Found {name} on the path {file}.")
        if name == FILE_NAMES["authors"]:
            if not check_authors(file):
                code = 1
        if name == FILE_NAMES["license"]:
            if not check_license(file, name):
                code = 1
    not_found = []
    for file in FILE_NAMES.values():
        if file not in found_file_names:
            not_found.append(file)
    if not_found:
        code = 1
        logger.error("Files " + ", ".join(not_found) + " not found.")
    if code == 0:
        logger.info("All markdown files have been verified.")
    return code


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to check markdown files"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    return parser.parse_args()


def check_license(file, name):
    result = False
    for line in open(file):
        if re.fullmatch(REGEX["copyright"], line.strip()):
            result = True
            break
    if not result:
        logger.error(f"No lines matching copyright regex in file {name}.")
    return result


def check_authors(file):
    if os.stat(file).st_size == 0:
        logger.error("File is empty.")
        return False
    check_authors_language(file)
    file_content = check_authors_header(file)
    if not file_content:
        return False
    authors_dictionary = split_authors(file_content)
    section_result = check_roles(authors_dictionary)
    return section_result


def check_authors_language(file):
    DetectorFactory.seed = 0
    with open(file) as opened_file:
        content = opened_file.read()
    content = re.sub(REGEX["sub_special"], " ", content)
    result = langdetect.detect_langs(content)
    if not len(result) == 1 or not result[0].lang == "en":
        logger.warning(f"Found languages other than English in {file}.")
    return


def check_authors_header(file):
    file_content = []
    with open(file) as opened_file:
        for line in opened_file:
            file_content.append(line.rstrip())
    if not file_content[0] == AUTHORS_HEADER:
        logger.error("Line 1 must be # Authors.")
        return False
    if file_content[1]:
        logger.error("Line 2 must be empty.")
        return False
    if not file_content[2]:
        logger.error("Line 3 must contain author, not whitespace.")
        return False
    file_content = file_content[2:]
    return file_content


def split_authors(file_content):
    authors_dict = {}
    current_key = None
    current_values = []
    for line in file_content:
        if re.fullmatch(REGEX["author_start"], line):
            if current_key:
                authors_dict[current_key] = {}
                authors_dict[current_key]["contents"] = current_values
                authors_dict[current_key]["line-number"] = (
                    file_content.index(current_key) + 1
                )
            current_key = line
            current_values = []
        else:
            current_values.append(line)
    if current_key:
        authors_dict[current_key] = {}
        authors_dict[current_key]["contents"] = current_values
        authors_dict[current_key]["line-number"] = file_content.index(current_key) + 1
    return authors_dict


def check_roles(dictionary):
    result = True
    for section in dictionary:
        file_index = dictionary[section]["line-number"] + 2
        if not re.fullmatch(REGEX["author"], section):
            logger.error(
                f"Line {file_index} with name does not match the requirements.",
            )
        full_content = dictionary[section]["contents"]
        for i, value in enumerate(full_content):
            if not re.fullmatch(REGEX["role"], value):
                logger.error(
                    f"Line {file_index + i + 1} with role does not match the requirements.",
                )
    return result


if __name__ == "__main__":
    sys.exit(main())
