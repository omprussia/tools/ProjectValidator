from dataclasses import dataclass
import os
import shutil
import subprocess
import sys
import logging.config
from streamformatter import StreamFormatter

LOGGER_DICT = {
    "version": 1,
    "formatters": {
        "stream": {
            "()": StreamFormatter,
        },
    },
    "handlers": {
        "stream": {
            "class": "logging.StreamHandler",
            "formatter": "stream",
            "stream": "ext://sys.stdout",
        },
    },
    "loggers": {"": {"level": "WARNING", "handlers": ["stream"]}},
}
LOGGING_LEVELS = ["INFO", "WARNING", "ERROR"]


@dataclass
class ExceptData:
    directories: list
    files: list


@dataclass
class ExceptArguments:
    except_check_cpp: bool
    except_check_qml: bool
    except_check_license: bool
    except_check_markdown: bool
    except_check_translation: bool
    except_check_pro: bool
    except_check_executable: bool


def add_and_apply_profile(
    except_check_cpp,
    except_check_qml,
    except_check_license,
    except_check_markdown,
    except_check_translation,
    except_check_pro,
    except_check_executable,
    profile,
):
    if profile is not None:
        if profile == "OnlyCodeCheck":
            except_check_license = True
            except_check_markdown = True
            except_check_translation = True
            except_check_pro = True
            except_check_executable = True
        elif profile == "DisableCppCheck":
            except_check_cpp = True
        elif profile == "DisableQmlCheck":
            except_check_qml = True
        elif profile == "DisableCodeCheck":
            except_check_cpp = True
            except_check_qml = True
        elif profile == "DisableMarkdownCheck":
            except_check_markdown = True
        else:
            tools_logger.error(
                f'Profile "{profile}" does not exist.',
            )
    return ExceptArguments(
        except_check_cpp,
        except_check_qml,
        except_check_license,
        except_check_markdown,
        except_check_translation,
        except_check_pro,
        except_check_executable,
    )


def get_logger():
    logging.config.dictConfig(LOGGER_DICT)
    return logging.getLogger(__name__)


tools_logger = get_logger()


def set_log_level(logger, level):
    if level in LOGGING_LEVELS:
        logger.setLevel(level)
        tools_logger.setLevel(level)


def check_directory(directory, directory_type=""):
    if not os.path.exists(directory):
        tools_logger.error(
            f'{directory_type} directory "{directory}" does not exist.',
        )
        return sys.exit(1)


def expand_and_check_subdirectories(base_directory, subdirectories):
    directories = list(
        map(
            lambda directory: os.path.join(base_directory, directory),
            subdirectories,
        )
    )
    for directory in directories:
        check_directory(directory)
    return directories


def list_directories(base_directory, excluded_directories, subdirectories=[]):
    base_directories = []
    directories = []
    excluded_directories = expand_and_check_subdirectories(
        base_directory, excluded_directories
    )
    if subdirectories:
        for directory in subdirectories:
            base_directories.append(os.path.join(base_directory, directory))
    else:
        base_directories.append(base_directory)
    for directory in base_directories:
        for dirpath, _dirnames, _filenames in os.walk(directory):
            if not any(
                os.path.abspath(dirpath).startswith(os.path.abspath(excluded_directory))
                for excluded_directory in excluded_directories
            ):
                directories.append(dirpath)
    for directory in directories:
        check_directory(directory)
    return directories


def list_files(
    base_directory,
    excluded_directories,
    excluded_files=[],
    file_extensions=[],
    file_names=(),
    subdirectories=[],
):
    target_files = []
    directories = list_directories(base_directory, excluded_directories, subdirectories)
    for directory in directories:
        for dirpath, _dirnames, filenames in os.walk(directory):
            for filename in filenames:
                full_path = os.path.join(dirpath, filename)
                if os.path.abspath(full_path) not in excluded_files:
                    if not file_extensions and not file_names:
                        target_files.append(full_path)
                    else:
                        if filename in file_names:
                            target_files.append(full_path)
                            continue
                        name_parts = os.path.splitext(filename)
                        if name_parts[1] in file_extensions:
                            target_files.append(full_path)
            break
    relative_target_files = map(
        lambda full_path: os.path.relpath(full_path, base_directory), target_files
    )
    return list(relative_target_files)


DEFAULT_EXCEPT_DIRS = [".apptool", ".git"]


def add_default_except_dir(base_directory):
    except_dir = []
    except_file = []
    for directory in DEFAULT_EXCEPT_DIRS:
        if os.path.isdir(os.path.join(base_directory, directory)):
            except_dir.append(directory)
    if (
        subprocess.call(
            ["git", "-C", base_directory, "status"],
            stderr=subprocess.STDOUT,
            stdout=open(os.devnull, "w"),
        )
        == 0
    ):
        gitignore_list = get_gitignore_list()
        for item in gitignore_list:
            full_path = os.path.join(base_directory, item)
            if os.path.isdir(full_path):
                if item not in except_dir:
                    except_dir.append(item)
            elif os.path.isfile(full_path):
                if full_path not in except_file:
                    except_file.append(full_path)
    return ExceptData(except_dir, except_file)


QML_FORMAT_KNOWN_LOCATIONS = [
    "/usr/lib/qt6/bin/qmlformat",
    "/usr/bin/qmlformat-qt6",
    shutil.which("qmlformat"),
    "/usr/lib/qt5/bin/qmlformat",
    "/usr/bin/qmlformat-qt5",
]


def find_executable(locations, tool):
    for qmlformat in locations:
        if (not qmlformat) or (qmlformat is None):
            continue
        if os.path.exists(qmlformat):
            return qmlformat
    tools_logger.error(f"{tool} executable was not found.")
    sys.exit(1)


QML_FORMAT = find_executable(QML_FORMAT_KNOWN_LOCATIONS, "qmlformat")


def change_ownership(base_directory, *files):
    stats = os.stat(base_directory)
    uid = stats.st_uid
    gid = stats.st_gid
    for filename in files:
        os.chown(filename, uid, gid)


CLANG_FORMAT_KNOWN_LOCATIONS = [
    shutil.which("clang-format-15"),
    "/usr/lib/llvm-15.0/bin/clang-format",
    shutil.which("clang-format"),
]

CLANG_FORMAT = find_executable(CLANG_FORMAT_KNOWN_LOCATIONS, "clang-format")


DEFAULT_CLANG_FORMAT_PATH = os.path.join(os.path.dirname(__file__), "clang-format")


def get_clang_format_path(base_directory):
    if os.path.exists(base_directory + "/.clang-format"):
        tools_logger.info(
            "The .clang-format configuration file was found in the root of the project. It will be used instead of the formatter configuration."
        )
        return base_directory + "/.clang-format"
    tools_logger.warning(
        "The .clang-format configuration file was not found. The formatter configuration file is used.",
    )
    return DEFAULT_CLANG_FORMAT_PATH


LRELASE_KNOWN_LOCATIONS = [
    shutil.which("lrelease"),
    "/usr/lib/qt6/bin/lrelease",
    "/usr/bin/lrelease-qt6",
]

LRELEASE = find_executable(LRELASE_KNOWN_LOCATIONS, "lrelease")


def get_gitignore_list():
    result = subprocess.run(
        [
            GIT,
            "ls-files",
            "--others",
            "--ignored",
            "--exclude-standard",
            "--directory",
        ],
        stdout=subprocess.PIPE,
        text=True,
    )
    return list(filter(None, result.stdout.split("\n")))


GIT_KNOWN_LOCATIONS = [
    shutil.which("git"),
    "/usr/lib/git-core/git",
    "/usr/bin/git",
]

GIT = find_executable(GIT_KNOWN_LOCATIONS, "git")
