#!/usr/bin/env python3

import argparse
from dataclasses import dataclass
import glob
import os
import re
import sys
import logging.config
from aurora_tools_common import (
    check_directory,
    list_files,
    add_default_except_dir,
    set_log_level,
    LOGGER_DICT,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
)

DISTFILES_REGEX = r"DISTFILES\s*\+=\s*(((?:[^\/]*\/)*)(\S+))?(\s*\\)?"
FILE_REGEX = r"((?:[^\/]*\/)*)(\S+)(\s+\\)?"
CLEAR_FILE_REGEX = r"((?:[^\/]*\/)*)([^\\\s]+)"
DISTFILES_SECTION_REGEX = r"DISTFILES\s*\+=\s"


logging.config.dictConfig(LOGGER_DICT)
logger = logging.getLogger(__name__)


@dataclass
class DistFiles:
    index: int
    paths: list

    def found(self) -> bool:
        return self.index > 0


def main():
    arguments = parse_arguments()
    if arguments.log_level:
        set_log_level(logger, arguments.log_level)
    if arguments.directory:
        check_directory(arguments.directory)
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    files = list_files(
        base_directory,
        except_data.directories,
        except_data.files,
        [".pro"],
    )
    if check_paths_in_dist_files(files):
        return 0
    return 1


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to check pro files"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    return parser.parse_args()


def check_paths_in_dist_files(files) -> bool:
    code = True
    for file in files:
        pro_file_contents = []
        with open(file) as opened_file:
            for line in opened_file:
                pro_file_contents.append(line.strip())
        dist_files = extract_dist_files(pro_file_contents)
        if dist_files.found():
            full_paths = get_full_path_to_dist_files(file, dist_files.paths)
            if not check_dist_files(file, dist_files, full_paths):
                code = False
        else:
            logger.warning(f"No DISTFILES section in {file}.")
    return code


def extract_dist_files(file_content) -> DistFiles:
    dist_files_index = 0
    dist_files_content = ()
    for count, line in enumerate(file_content):
        if re.fullmatch(DISTFILES_REGEX, line.strip()):
            dist_files_index = count + 1
            dist_files_content = file_content[count:]
            break
    if dist_files_index > 0:
        for count, line in enumerate(dist_files_content[1:], 1):
            if not re.fullmatch(FILE_REGEX, line):
                dist_files_content = dist_files_content[0:count]
                break
    return DistFiles(dist_files_index, dist_files_content)


def get_full_path_to_dist_files(file, paths):
    result = []
    pro_file_dir_name = os.path.dirname(file)
    first_path_match = re.search(
        CLEAR_FILE_REGEX, re.sub(DISTFILES_SECTION_REGEX, "", paths[0], 1)
    )
    if first_path_match:
        result.append(os.path.join(pro_file_dir_name, first_path_match.group()))
    else:
        result.append("")
    for count, line in enumerate(paths[1:]):
        path_match = re.search(CLEAR_FILE_REGEX, line)
        if path_match:
            result.append(os.path.join(pro_file_dir_name, path_match.group()))
        else:
            result.append("")
    return result


def check_dist_files(pro_file, dist_files, full_paths):
    code = True
    for count, path in enumerate(full_paths):
        if path:
            if glob.glob(check_pwd_in_path_dist_file(path)):
                logger.info(
                    f"File {path} found in {pro_file} on line {dist_files.index + count} exists."
                )
            else:
                logger.error(
                    f"File {path} found in {pro_file} on line {dist_files.index + count} does not exist."
                )
                code = False
    return code


def check_pwd_in_path_dist_file(path):
    pwd_val = "$$PWD"
    if pwd_val in path:
        return path.replace(pwd_val, os.getcwd(), 1)
    return path


if __name__ == "__main__":
    sys.exit(main())
