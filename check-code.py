#!/usr/bin/env python3

import argparse
import os
import sys
import subprocess
from aurora_tools_common import add_and_apply_profile
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
    add_format_argument,
    add_cpp_dir_argument,
    add_qml_dir_argument,
    add_license_path_argument,
    add_year_argument,
    add_fix_ownership_argument,
    add_except_check_cpp_argument,
    add_except_check_qml_argument,
    add_except_check_license_argument,
    add_except_check_markdown_argument,
    add_except_check_translation_argument,
    add_except_check_pro_argument,
    add_except_check_executable_argument,
    add_profile_argument,
)


def main():
    arguments = parse_arguments()
    if arguments.directory:
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_arguments = add_and_apply_profile(
        arguments.except_check_cpp,
        arguments.except_check_qml,
        arguments.except_check_license,
        arguments.except_check_markdown,
        arguments.except_check_translation,
        arguments.except_check_pro,
        arguments.except_check_executable,
        arguments.profile,
    )
    if check_files(
        base_directory,
        arguments.format,
        arguments.cpp_dir,
        arguments.qml_dir,
        arguments.except_dir,
        arguments.year,
        arguments.license_path,
        arguments.fix_ownership,
        arguments.log_level,
        except_arguments,
    ):
        return 0
    return 1


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to check code for Aurora OS"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    add_format_argument(parser)
    add_cpp_dir_argument(parser)
    add_qml_dir_argument(parser)
    add_license_path_argument(parser)
    add_year_argument(parser)
    add_fix_ownership_argument(parser)
    add_except_check_cpp_argument(parser)
    add_except_check_qml_argument(parser)
    add_except_check_license_argument(parser)
    add_except_check_markdown_argument(parser)
    add_except_check_translation_argument(parser)
    add_except_check_pro_argument(parser)
    add_except_check_executable_argument(parser)
    add_profile_argument(parser)
    return parser.parse_args()


def check_files(
    directory,
    start_format,
    cpp_dirs,
    qml_dirs,
    except_dirs,
    year,
    license_path,
    fix_ownership,
    log_level,
    except_arguments,
):
    results = True
    if start_format:
        if not except_arguments.except_check_cpp:
            results = (
                format_cpp_files(directory, cpp_dirs, except_dirs, fix_ownership)
                and results
            )
        if not except_arguments.except_check_qml:
            results = (
                format_qml_files(directory, qml_dirs, except_dirs, fix_ownership)
                and results
            )
    else:
        if not except_arguments.except_check_cpp:
            results = (
                check_cpp_files(directory, cpp_dirs, except_dirs, log_level) and results
            )
        if not except_arguments.except_check_qml:
            results = (
                check_qml_files(directory, qml_dirs, except_dirs, log_level) and results
            )
    if not except_arguments.except_check_license:
        results = (
            check_license_header(directory, year, license_path, except_dirs, log_level)
            and results
        )
    if not except_arguments.except_check_markdown:
        results = check_markdown_files(directory, except_dirs, log_level) and results
    if not except_arguments.except_check_translation:
        results = check_translation_files(directory, except_dirs, log_level) and results
    if not except_arguments.except_check_pro:
        results = check_pro_files(directory, except_dirs, log_level) and results
    if not except_arguments.except_check_executable:
        results = check_executable(directory, except_dirs, log_level) and results
    return results


BIN_DIR = os.path.abspath(os.path.dirname(__file__))

CPP_CHECK = os.path.join(BIN_DIR, "check-cpp-code.py")


def check_cpp_files(directory, cpp_dirs, except_dirs, log_level):
    result = subprocess.run(
        [
            CPP_CHECK,
            "--directory",
            directory,
            "--cpp-dir",
            *cpp_dirs,
            "--except-dir",
            *except_dirs,
            "--log-level",
            log_level,
        ]
    )
    return result.returncode == 0


CPP_FORMAT = os.path.join(BIN_DIR, "format-cpp-code.py")


def format_cpp_files(directory, cpp_dirs, except_dirs, fix_ownership):
    args = [
        CPP_FORMAT,
        "--directory",
        directory,
        "--cpp-dir",
        *cpp_dirs,
        "--except-dir",
        *except_dirs,
    ]
    if fix_ownership:
        args.append("--fix-ownership")
    result = subprocess.run(args)
    return result.returncode == 0


QML_CHECK = os.path.join(BIN_DIR, "check-qml-code.py")


def check_qml_files(directory, qml_dirs, except_dirs, log_level):
    result = subprocess.run(
        [
            QML_CHECK,
            "--directory",
            directory,
            "--qml-dir",
            *qml_dirs,
            "--except-dir",
            *except_dirs,
            "--log-level",
            log_level,
        ]
    )
    return result.returncode == 0


QML_FORMAT = os.path.join(BIN_DIR, "format-qml-code.py")


def format_qml_files(directory, qml_dirs, except_dirs, fix_ownership):
    args = [
        QML_FORMAT,
        "--directory",
        directory,
        "--qml-dir",
        *qml_dirs,
        "--except-dir",
        *except_dirs,
    ]
    if fix_ownership:
        args.append("--fix-ownership")
    result = subprocess.run(args)
    return result.returncode == 0


HEADER_CHECK = os.path.join(BIN_DIR, "license-format.py")


def check_license_header(directory, year, license_path, except_dirs, log_level):
    args = [
        HEADER_CHECK,
        "--directory",
        directory,
        "--year",
        year,
        "--license-path",
        license_path,
        "--except-dir",
        *except_dirs,
        "--check-license",
        "--log-level",
        log_level,
    ]
    result = subprocess.run(args)
    return result.returncode == 0


TRANSLATION_CHECK = os.path.join(BIN_DIR, "check-translation.py")


def check_translation_files(directory, except_dirs, log_level):
    result = subprocess.run(
        [
            TRANSLATION_CHECK,
            "--directory",
            directory,
            "--except-dir",
            *except_dirs,
            "--log-level",
            log_level,
        ]
    )
    return result.returncode == 0


MARKDOWN_CHECK = os.path.join(BIN_DIR, "check-md-files.py")


def check_markdown_files(directory, except_dirs, log_level):
    result = subprocess.run(
        [
            MARKDOWN_CHECK,
            "--directory",
            directory,
            "--except-dir",
            *except_dirs,
            "--log-level",
            log_level,
        ]
    )
    return result.returncode == 0


PRO_CHECK = os.path.join(BIN_DIR, "check-pro-files.py")


def check_pro_files(directory, except_dirs, log_level):
    result = subprocess.run(
        [
            PRO_CHECK,
            "--directory",
            directory,
            "--except-dir",
            *except_dirs,
            "--log-level",
            log_level,
        ]
    )
    return result.returncode == 0


EXECUTABLE_CHECK = os.path.join(BIN_DIR, "remove-executable-bit.py")


def check_executable(directory, except_dirs, log_level):
    result = subprocess.run(
        [
            EXECUTABLE_CHECK,
            "--directory",
            directory,
            "--except-dir",
            *except_dirs,
            "--log-level",
            log_level,
        ]
    )
    return result.returncode == 0


if __name__ == "__main__":
    sys.exit(main())
