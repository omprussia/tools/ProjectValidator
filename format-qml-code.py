#!/usr/bin/env python3

import argparse
import os
import sys
import subprocess
from aurora_tools_common import (
    check_directory,
    list_files,
    QML_FORMAT,
    change_ownership,
    add_default_except_dir,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_qml_dir_argument,
    add_fix_ownership_argument,
)


def main():
    arguments = parse_arguments()
    if arguments.directory:
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    qml_files = list_files(
        base_directory,
        except_data.directories,
        except_data.files,
        [".qml"],
        subdirectories=arguments.qml_dir,
    )
    if len(qml_files) == 0:
        return 0
    temp_qml_files = []
    for file in qml_files:
        temp_qml_files.append(file + "~")
    result = format_qml_files(qml_files)
    for temp_file in temp_qml_files:
        if os.path.isfile(temp_file):
            os.remove(temp_file)
    if arguments.fix_ownership:
        change_ownership(base_directory, *qml_files)
    if result:
        return 0
    return 1


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to format code with qmlformat"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_qml_dir_argument(parser)
    add_fix_ownership_argument(parser)
    return parser.parse_args()


def format_qml_files(qml_files):
    result = subprocess.run(
        [
            QML_FORMAT,
            "-i",
            *qml_files,
        ]
    )
    return result.returncode == 0


if __name__ == "__main__":
    sys.exit(main())
