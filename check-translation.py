#!/usr/bin/env python3

import argparse
import os
import shutil
import sys
import subprocess
import tempfile
import logging.config
from lxml import etree
from aurora_tools_common import (
    check_directory,
    list_files,
    LRELEASE,
    add_default_except_dir,
    set_log_level,
    LOGGER_DICT,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
)

logging.config.dictConfig(LOGGER_DICT)
logger = logging.getLogger(__name__)


def main():
    arguments = parse_arguments()
    if arguments.log_level:
        set_log_level(logger, arguments.log_level)
    if arguments.directory:
        check_directory(arguments.directory, "Base")
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    translation_files = list_files(
        base_directory, except_data.directories, except_data.files, [".ts"]
    )
    if len(translation_files) == 0:
        return 0
    result_check_existence = check_existence(translation_files)
    result_check_syntactic = check_syntactic(translation_files)
    result_check_translation = check_translation(translation_files)
    if result_check_existence and result_check_syntactic and result_check_translation:
        return 0
    return 1


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to check Qt translation files"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    return parser.parse_args()


def check_existence(translation_files):
    logger.info("Checking the availability of translation files...")
    if not any(file.endswith("-ru.ts") for file in translation_files):
        logger.error(
            "You need to add '-ru.ts' file containing translations into Russian.",
        )
        return False
    return True


def check_syntactic(translation_files):
    logger.info("Checking the syntactic data of translation files...")
    temp_translation_files = []
    with tempfile.TemporaryDirectory() as qm_dir:
        for file in translation_files:
            shutil.copy(file, qm_dir)
            temp_translation_files.append(os.path.join(qm_dir, os.path.basename(file)))
        result = subprocess.run(
            [
                LRELEASE,
                "-silent",
                *temp_translation_files,
            ],
            stderr=subprocess.PIPE,
            text=True,
        )
        lrelease_err = result.stderr.replace(qm_dir + "/", "")
    if lrelease_err:
        logger.error(lrelease_err)
    return result.returncode == 0


def check_translation(translation_files):
    logger.info("Checking for translations in translation files...")
    error_check = True
    for file in translation_files:
        with open(file) as fobj:
            ts_file = fobj.read()
        root_node = etree.fromstring(bytes(ts_file, encoding="utf-8"))
        for message in root_node.findall("context/message"):
            if find_error_in_node(message, os.path.basename(file)) and error_check:
                error_check = False
    return error_check


def get_attribute(tag, name_attrib):
    if tag is not None:
        return tag.get(name_attrib)
    else:
        return None


def find_error_in_node(node, ts_file):
    translation = node.find("translation")
    type_translation = get_attribute(translation, "type")
    text_translation = translation.text
    line_translation = translation.sourceline
    location = node.find("location")
    filename_location = get_attribute(location, "filename")
    line_location = get_attribute(location, "line")
    if type_translation == "unfinished":
        logger.error(
            f"In the '{ts_file}' file on line '{line_translation}': translation is unfinished.\nThe location of the translation: filename '{filename_location}' line '{line_location}'.",
        )
        return True
    elif text_translation is None:
        logger.error(
            f"In the '{ts_file}' file on line '{line_translation}': there is no translation.\nThe location of the translation: filename '{filename_location}' line '{line_location}'.",
        )
        return True
    return False


if __name__ == "__main__":
    sys.exit(main())
