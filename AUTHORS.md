# Authors

* Andrey Vasilyev
  * Developer, 2024
* Oleg Shevchenko
  * Developer, 2024
* Roman Mazaev
  * Developer, 2024
* Andrej Begichev <a.begichev@omp.ru>
  * Maintainer, 2024
  * Reviewer, 2024
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
