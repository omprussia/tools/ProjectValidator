#!/usr/bin/env python3

import argparse
import os
import stat
import sys
import logging.config
from aurora_tools_common import (
    check_directory,
    list_files,
    add_default_except_dir,
    set_log_level,
    LOGGER_DICT,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
    add_check_executable_argument,
)

logging.config.dictConfig(LOGGER_DICT)
logger = logging.getLogger(__name__)


def main():
    arguments = parse_arguments()
    if arguments.log_level:
        set_log_level(logger, arguments.log_level)
    if arguments.directory:
        check_directory(arguments.directory)
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    files = list_files(
        base_directory,
        except_data.directories,
        except_data.files,
    )
    logger.info("Checking for the executable bit in files...")
    code = find_executable_files(files, arguments.check_executable)
    return code


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to remove executable bit from files"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    add_check_executable_argument(parser)
    return parser.parse_args()


def find_executable_files(files, check_executable):
    code = 0
    for file in files:
        current_bits = os.stat(file).st_mode
        if (
            bool(current_bits & stat.S_IXUSR)
            or bool(current_bits & stat.S_IXGRP)
            or bool(current_bits & stat.S_IXOTH)
        ):
            logger.error(f"File {file} is executable.")
            code = 1
            if not check_executable:
                remove_executable_bit(file, current_bits)
    return code


def remove_executable_bit(file, current_bits):
    os.chmod(
        file, stat.S_IMODE(current_bits) & ~stat.S_IXUSR & ~stat.S_IXGRP & ~stat.S_IXOTH
    )


if __name__ == "__main__":
    sys.exit(main())
