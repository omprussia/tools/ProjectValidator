import os
import datetime


def add_directory_argument(parser):
    parser.add_argument(
        "--directory",
        help="the directory to process the files",
        default=".",
    )


def add_except_dir_argument(parser):
    parser.add_argument(
        "--except-dir",
        help="list of directory names to ignore",
        nargs="*",
        default=[],
    )


def add_log_level_argument(parser):
    parser.add_argument(
        "--log-level",
        help="level of messages to print to stdout (info, warning, error)",
        type=str.upper,
        default="",
    )


def add_format_argument(parser):
    parser.add_argument(
        "--format",
        help="start code formatting",
        action="store_true",
        default=False,
    )


def add_cpp_dir_argument(parser):
    parser.add_argument(
        "--cpp-dir",
        help="CPP subdirectories to process",
        nargs="+",
        default=["."],
    )


def add_qml_dir_argument(parser):
    parser.add_argument(
        "--qml-dir",
        help="QML subdirectories to process",
        nargs="+",
        default=["."],
    )


def add_license_path_argument(parser):
    parser.add_argument(
        "--license-path",
        help="path for file with license header",
        default=f"{os.path.dirname(__file__)}/license-template",
    )


def add_year_argument(parser):
    parser.add_argument(
        "--year",
        help="year for license header, default is current year",
        default=f"{datetime.date.today().year}",
    )


def add_fix_ownership_argument(parser):
    parser.add_argument(
        "--fix-ownership",
        help="fix ownership of the processed files",
        action="store_true",
        default=False,
    )


def add_check_license_argument(parser):
    parser.add_argument(
        "--check-license",
        help="only checks license header without writing it in file",
        action="store_true",
        default=False,
    )


def add_replace_license_argument(parser):
    parser.add_argument(
        "--replace-license",
        help="replace old license header with a new one",
        action="store_true",
        default=False,
    )


def add_check_executable_argument(parser):
    parser.add_argument(
        "--check-executable",
        help="only checks executable bit in files",
        action="store_true",
        default=False,
    )


def add_except_check_cpp_argument(parser):
    parser.add_argument(
        "--except-check-cpp",
        help="exclude script for checking cpp files from check",
        action="store_true",
        default=False,
    )


def add_except_check_qml_argument(parser):
    parser.add_argument(
        "--except-check-qml",
        help="exclude script for checking qml files from check",
        action="store_true",
        default=False,
    )


def add_except_check_license_argument(parser):
    parser.add_argument(
        "--except-check-license",
        help="exclude script for checking license header from check",
        action="store_true",
        default=False,
    )


def add_except_check_markdown_argument(parser):
    parser.add_argument(
        "--except-check-markdown",
        help="exclude script for checking markdown files from check",
        action="store_true",
        default=False,
    )


def add_except_check_translation_argument(parser):
    parser.add_argument(
        "--except-check-translation",
        help="exclude script for checking translation files from check",
        action="store_true",
        default=False,
    )


def add_except_check_pro_argument(parser):
    parser.add_argument(
        "--except-check-pro",
        help="exclude script for checking pro files from check",
        action="store_true",
        default=False,
    )


def add_except_check_executable_argument(parser):
    parser.add_argument(
        "--except-check-executable",
        help="exclude script for removing executable bit from files ",
        action="store_true",
        default=False,
    )


def add_profile_argument(parser):
    parser.add_argument(
        "--profile",
        help="use profile to exclude different checks",
        type=str,
    )
