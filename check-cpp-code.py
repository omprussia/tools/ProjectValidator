#!/usr/bin/env python3

import argparse
import os
import sys
import subprocess
import logging.config
from aurora_tools_common import (
    check_directory,
    list_files,
    CLANG_FORMAT,
    get_clang_format_path,
    add_default_except_dir,
    set_log_level,
    LOGGER_DICT,
)
from arguments import (
    add_directory_argument,
    add_except_dir_argument,
    add_log_level_argument,
    add_cpp_dir_argument,
)

logging.config.dictConfig(LOGGER_DICT)
logger = logging.getLogger(__name__)


def main():
    arguments = parse_arguments()
    if arguments.log_level:
        set_log_level(logger, arguments.log_level)
    if arguments.directory:
        os.chdir(arguments.directory)
    base_directory = os.getcwd()
    except_data = add_default_except_dir(base_directory)
    except_data.directories.extend(arguments.except_dir)
    check_directory(base_directory, "Base")
    cpp_files = list_files(
        base_directory,
        except_data.directories,
        except_data.files,
        [".h", ".cpp"],
        subdirectories=arguments.cpp_dir,
    )
    if len(cpp_files) == 0:
        return 0
    clang_format_path = get_clang_format_path(base_directory)
    if check_cpp_files(cpp_files, clang_format_path):
        return 0
    return 1


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.description = "Tool to check code with clang-format"
    add_directory_argument(parser)
    add_except_dir_argument(parser)
    add_log_level_argument(parser)
    add_cpp_dir_argument(parser)
    return parser.parse_args()


def check_cpp_files(cpp_files, clang_format_path):
    clang_info = subprocess.run([CLANG_FORMAT, "--version"], capture_output=True)
    logger.info(f"Checking with {clang_info.stdout.decode('UTF-8')}.".replace("\n", ""))
    style = f"-style=file:{clang_format_path}"
    result = subprocess.run(
        [
            CLANG_FORMAT,
            style,
            "--dry-run",
            "-Werror",
            *cpp_files,
        ],
    )
    return result.returncode == 0


if __name__ == "__main__":
    sys.exit(main())
